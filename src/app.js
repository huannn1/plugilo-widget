import { h, render } from 'preact';

import style from "./theme.scss";
import HelloWorld from './views/HelloWorld';

export function run(options = {}) {

    // render(<HelloWorld />, options.target);
    render(<HelloWorld handleClick={options.onClick} />, document.body);
};