import { h, render, Component } from 'preact';
import Postmate from 'postmate';
// Postmate.debug = true;

export default class HelloWorld extends Component {
    state = { todos: [], text: '' };
    handshake = null;

    setText = e => {
        this.setState({ text: e.target.value });
    };
    addTodo = () => {
        let { todos, text } = this.state;
        todos = todos.concat({ text });
        this.setState({ todos, text: '' });

        this.updateIframeHeight();
    };
    handleClick = () => {
        console.log("Before click");
        const callback = this.props.onClick;
        if (typeof callback === 'function')
            callback();
    };
    componentDidMount() {
        this.handshake = new Postmate.Model({
            // Expose your model to the Parent. Property values may be functions, promises, or regular values
            height: () => Math.max(150, document.height || document.body.scrollHeight)
        });

        // When parent <-> child handshake is complete, events may be emitted to the parent
        this.handshake.then(parent => {
            parent.emit('some-event', 'Hello, World!');
        });
    }
    updateIframeHeight() {
        this.handshake.then(parent => {
            parent.emit('set-height', document.height || document.body.scrollHeight);
        });
    }
    render({ }, { todos, text }) {
        return (
            <form onSubmit={this.addTodo} action="javascript:">
                <input value={text} onInput={this.setText} />
                <button type="submit" class="btn">Add</button>
                {/* <button type="button" class="btn" onClick={this.handleClick}>Test postMessage</button> */}
                <ul>
                    {todos.map(todo => (
                        <li>{todo.text}</li>
                    ))}
                </ul>
            </form>
        );
    }
}