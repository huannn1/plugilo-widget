import Postmate from 'postmate';

// Postmate.debug = true;
export function initialize(options = {}) {
    const handshake = new Postmate({
        container: document.body, // Element to inject frame into
        url: '/app.html', // Page to load, must have postmate.js. This will also be the origin used for communication.
        classListArray: ["myClass"] //Classes to add to the iframe via classList, useful for styling.
    });

    // When parent <-> child handshake is complete, data may be requested from the child
    handshake.then(child => {

        // Fetch the height property in child.html and set it to the iFrames height
        child.get('height')
            .then(height => child.frame.style.height = `${height}px`);

        child.on('set-height', height => child.frame.style.height = `${height}px`);

        // Listen to a particular event from the child
        child.on('some-event', data => console.log(data)); // Logs "Hello, World!"
    });

    // const sandbox = document.createElement('iframe');
    // sandbox.srcdoc = `
    //   <body>
    //   <script src="/app.plugilowidget.js"></script><script>PlugiloWidget.initialize()</script></body>
    // `;
    // document.body.appendChild(sandbox);

    //     var iframe = document.createElement("iframe");
    //     var html = `<body><script>
    //     // Ughh, cleanup, validate, parse, etc options' attrs!
    //     function initializeWidget(){ PlugiloWidget.initialize() }
    //   </script>
    //   <script src="/app.plugilowidget.js"/></script><script>PlugiloWidget.initialize()</script></body>`
    //     document.body.appendChild(iframe);
    //     iframe.className = "content-view-slot-body";
    //     iframe.style = "border: none";
    //     iframe.contentWindow.document.open();
    //     iframe.contentWindow.document.write(html);
    //     iframe.contentWindow.document.close();
    //     console.log("Init done");
};