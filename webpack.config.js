const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: {
        app: './src/app.js',
        widget: './src/widget.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),   // An absolute path to the output directory
        filename: '[name].plugilowidget.js',                   // The name to use for the output file
        library: 'PlugiloWidget',   // Name of the library
        libraryTarget: 'umd'   // Make it available to all sorts of environments
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),      // Do not emit the bundle if there are errors while compiling
        new webpack.HotModuleReplacementPlugin(), // Enable HMR, of course,
    ],
    module: {
        rules: [{
            test: /\.js$/,                              // Compile all .js files with Babel
            exclude: /(node_modules|bower_components)/, // Do not transform npm  and bower packages, add other unwanted sources
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/env',
                        // '@babel/preset-react'
                    ]
                },
            }
        }, {
            test: /\.(scss|css)$/,            // Move all the required *.css and *.scss modules in entry chunks into a separate CSS file
            use: [
                // { loader: 'style-loader/url' },
                // { loader: 'file-loader' },
                { loader: 'style-loader' },
                { loader: "css-loader" },
                { loader: "sass-loader" }
            ],
        }]
    }
};