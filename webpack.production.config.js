// ./webpack.production.config.js
const webpack = require('webpack');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.config.js');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = merge(baseConfig, {
    optimization: {
        minimizer: [new UglifyJsPlugin()],
    },
});